object Versions {
    // kotlin & gradle defaults
    const val gradle = "4.1.2"
    const val kotlin = "1.4.0"

    // multidex
    const val multidex = "2.0.0"

    // android defaults
    const val appCompat = "1.2.0"
    const val coreKtx = "1.3.2"
    const val material = "1.3.0"

    // rx
    const val rxAndroid = "2.1.1"
    const val rxKotlin = "2.4.0"
    const val rxJava = "2.2.11"

    // retrofit + okhttp
    const val retrofit = "2.9.0"
    const val rxAdapter = "2.6.1"
    const val loggingInterceptor = "4.9.1"
    const val rxAdapterJava = "1.0.0"

    // moshi
    const val moshi = "1.9.2"

    // view model
    const val lifeCycle = "2.3.1"
    const val lifecycleReactiveStreams = "2.1.0"

}