object SDKDependencies {
    // stdlib
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"

    const val gradlePlugin = "com.android.tools.build:gradle:${Versions.gradle}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"

    // multidex
    private const val multidex = "androidx.multidex:multidex:${Versions.multidex}"

    // android ui
    private const val appcompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    private const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    private const val material = "com.google.android.material:material:${Versions.material}"

    // rx
    private const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    private const val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin}"
    private const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"

    // retrofit + okhttp
    private const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    private const val moshiKotlin = "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    private const val moshiAdapter = "com.squareup.moshi:moshi-adapters:${Versions.moshi}"
    private const val moshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    private const val rxAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.rxAdapter}"
    private const val loggingInterceptor =
        "com.squareup.okhttp3:logging-interceptor:${Versions.loggingInterceptor}"
    private const val rxAdapterJava =
        "com.jakewharton.retrofit:retrofit2-rxjava2-adapter:${Versions.rxAdapterJava}"

    // view model
    private const val runtime = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifeCycle}"
    private const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifeCycle}"
    private const val liveDate = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifeCycle}"
    private const val process = "androidx.lifecycle:lifecycle-process:${Versions.lifeCycle}"
    private const val reactiveStreams = "androidx.lifecycle:lifecycle-reactivestreams-ktx:${Versions.lifecycleReactiveStreams}"
    const val lifeCycle = "androidx.lifecycle:lifecycle-compiler:${Versions.lifeCycle}"


    val androidLibraries = listOf(kotlinStdLib, coreKtx, appcompat, material, multidex)

    val rxLibraries = listOf(rxAndroid, rxKotlin, rxJava)

    val networkLibraries = listOf(
        retrofit,
        moshiKotlin,
        moshiAdapter,
        moshiConverter,
        rxAdapter,
        loggingInterceptor,
        rxAdapterJava
    )


    val liveDataLibraries = listOf(
        runtime,
        viewModel,
        liveDate,
        process,
        reactiveStreams
    )

}