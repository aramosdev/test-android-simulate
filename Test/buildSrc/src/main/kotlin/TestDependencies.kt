object TestDependencies {
    private val junit = "junit:junit:${TestVersions.junit}"
    private val mockk = "io.mockk:mockk:${TestVersions.mockk}"
    private val extJUnit = "androidx.test.ext:junit:${TestVersions.extJunit}"
    private val espressoCore = "androidx.test.espresso:espresso-core:${TestVersions.espresso}"
    private val truth = "com.google.truth:truth:${TestVersions.truth}"
    private val archCore = "androidx.arch.core:core-testing:${TestVersions.archCore}"

    val testLibraries = listOf(junit, mockk, truth, archCore)

    val androidTestLibraries = listOf(extJUnit, espressoCore)
}