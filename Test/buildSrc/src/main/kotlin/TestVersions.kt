object TestVersions {
    const val junit = "4.13.2"
    const val extJunit = "1.1.2"
    const val espresso = "3.3.0"
    const val mockk = "1.10.0"
    const val truth = "1.1.2"
    const val archCore = "2.1.0"
}