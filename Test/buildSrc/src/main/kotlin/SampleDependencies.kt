object SampleDependencies {
    private const val dagger = "com.google.dagger:dagger:${SampleVersions.dagger}"
    private const val daggerAndroid = "com.google.dagger:dagger-android:${SampleVersions.dagger}"
    private const val daggerSupport = "com.google.dagger:dagger-android-support:${SampleVersions.dagger}"

    private const val compiler = "com.google.dagger:dagger-compiler:${SampleVersions.dagger}"
    private const val processor = "com.google.dagger:dagger-android-processor:${SampleVersions.dagger}"

    val appSample = listOf(dagger,daggerAndroid,daggerSupport)

    val daggerCompilers = listOf(compiler, processor)
}