object Config {
    const val applicationId = "br.com.aramosdev.mobile.test"
    const val compileSdk = 30
    const val minSdk = 16
    const val targetSdk = 30
    const val versionCode = 1
    const val versionName = "1.0.0"
    const val buildToolsVersion = "29.0.3"
    const val androidTestInstrumentation = "androidx.test.runner.AndroidJUnitRunner"
}