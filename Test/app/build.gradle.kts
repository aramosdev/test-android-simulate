repositories {
    google()  // For the Android support libraries.
}

android {
    packagingOptions {
        exclude("META-INF/*.kotlin_module")
    }

    buildTypes {
        getByName("debug") {
            buildConfigField("String", "BASE_URL", "\" https://run.mocky.io/\"")
        }
    }
}
dependencies {
    implementation(SampleDependencies.appSample)
    kapt(SampleDependencies.daggerCompilers)
    kapt(SDKDependencies.rxLibraries)
    implementation(SDKDependencies.androidLibraries)
    implementation(SDKDependencies.rxLibraries)
    implementation(SDKDependencies.networkLibraries)
    implementation(SDKDependencies.liveDataLibraries)

    testImplementation(TestDependencies.testLibraries)
    androidTestImplementation(TestDependencies.androidTestLibraries)

    kapt(listOf(SDKDependencies.lifeCycle))
    releaseImplementation("com.readystatesoftware.chuck:library-no-op:1.1.0")
    debugImplementation("com.readystatesoftware.chuck:library:1.1.0")
}

