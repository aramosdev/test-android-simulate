package br.com.aramosdev.test.simulator.domain

import br.com.aramosdev.test.simulator.data.SimulateRepository
import br.com.aramosdev.test.simulator.domain.models.Simulate
import br.com.aramosdev.test.simulator.domain.models.mapper.toSimulator
import io.reactivex.Single
import javax.inject.Inject

interface SimulateInteractor {
    fun simulateInvestment(investedAmount: Double,
                           index: String,
                           rate: Double,
                           maturityDate: String,
                           isTaxFree: Boolean): Single<Simulate>
}

class SimulateInteractorImpl @Inject constructor(
    private val repository: SimulateRepository
) : SimulateInteractor {

    override fun simulateInvestment(
        investedAmount: Double,
        index: String,
        rate: Double,
        maturityDate: String,
        isTaxFree: Boolean
    ): Single<Simulate> = repository
        .simulateInvestment(investedAmount, index, rate, maturityDate, isTaxFree).map {
            it.toSimulator()
        }
}