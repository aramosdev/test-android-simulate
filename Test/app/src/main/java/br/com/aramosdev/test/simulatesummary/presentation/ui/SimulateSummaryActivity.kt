package br.com.aramosdev.test.simulatesummary.presentation.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import br.com.aramosdev.test.R
import br.com.aramosdev.test.databinding.ActivitySimulateSuymmaryBinding
import br.com.aramosdev.test.shared.extention.bind
import br.com.aramosdev.test.shared.viewmodel.BaseActivity
import br.com.aramosdev.test.simulatesummary.presentation.viewmodel.SimulateSummaryViewModel
import br.com.aramosdev.test.simulatesummary.presentation.viewmodel.SimulateSummaryViewModelState
import br.com.aramosdev.test.simulator.domain.models.Simulate
import javax.inject.Inject

class SimulateSummaryActivity @Inject constructor() : BaseActivity() {

    private val summaryViewModel by appViewModel<SimulateSummaryViewModel>()
    private lateinit var binding: ActivitySimulateSuymmaryBinding

    private val simulateResult by lazy {
        intent.getParcelableExtra<Simulate>(BUNDLE_SIMULATE_RESULT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = bind(R.layout.activity_simulate_suymmary) {
            viewmodel = summaryViewModel
            lifecycleOwner = this@SimulateSummaryActivity
        }

        summaryViewModel.summaryResult.value = simulateResult

        watchState()
    }

    private fun watchState() {
        summaryViewModel.navigationState.observe(this) { event ->
            when (event) {
                is SimulateSummaryViewModelState.OpenSimulateAgain -> {
                    setResult(RESULT_OK)
                    finish()
                }
            }
        }
    }

    companion object {
        private const val BUNDLE_SIMULATE_RESULT = "BUNDLE_SIMULATE_RESULT"

        fun startActivity(context: BaseActivity, simulate: Simulate, requestCode: Int) {
            context.startActivityForResult(newIntent(context).apply {
                putExtra(BUNDLE_SIMULATE_RESULT, simulate)
            }, requestCode)
        }

        private fun newIntent(context: Context): Intent {
            return Intent(context, SimulateSummaryActivity::class.java)
        }
    }
}