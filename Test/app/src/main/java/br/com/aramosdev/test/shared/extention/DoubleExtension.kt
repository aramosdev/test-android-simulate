package br.com.aramosdev.test.shared.extention

import br.com.aramosdev.test.components.formatters.MoneyFormatter
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

private val formatterCurrency: NumberFormat by lazy {
    NumberFormat.getCurrencyInstance(Locale("pt", "BR"))
}

fun toMoney(value: Double?) = value.toMoney()

fun Double?.toMoney(
    positivePrefix: String = MoneyFormatter.CURRENCY,
    negativePrefix: String = MoneyFormatter.NEGATIVE_PREFIX
): String = this?.let {
    (formatterCurrency as DecimalFormat).positivePrefix = positivePrefix
    (formatterCurrency as DecimalFormat).negativePrefix = negativePrefix
    formatterCurrency.format(this)
} ?: kotlin.run {
    (formatterCurrency as DecimalFormat).positivePrefix = positivePrefix
    formatterCurrency.format(0.0)
}

fun Double.toPercentage() = "$this%"