package br.com.aramosdev.test.simulator.presentation.viewmodel

import br.com.aramosdev.test.simulator.domain.models.Simulate

sealed class SimulateViewModelState {
    data class ShowDetailSimulate(val simulate: Simulate) : SimulateViewModelState()
}

sealed class SimulateViewModelEvent {

    sealed class Loading :SimulateViewModelEvent() {
        object Show: Loading()
        object Hide: Loading()
    }

    sealed class Error :SimulateViewModelEvent() {
        object Default :Error()
    }
}