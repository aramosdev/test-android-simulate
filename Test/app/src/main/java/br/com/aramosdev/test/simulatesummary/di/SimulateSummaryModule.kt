package br.com.aramosdev.test.simulatesummary.di

import androidx.lifecycle.ViewModel
import br.com.aramosdev.test.shared.viewmodel.ViewModelKey
import br.com.aramosdev.test.simulatesummary.presentation.ui.SimulateSummaryActivity
import br.com.aramosdev.test.simulatesummary.presentation.viewmodel.SimulateSummaryViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class SimulateSummaryContributeModule {

    @ContributesAndroidInjector(modules = [(SimulateSummaryModule::class)])
    abstract fun provideSimulateSummaryActivity(): SimulateSummaryActivity
}

@Module
abstract class SimulateSummaryModule {

    @Binds
    @IntoMap
    @ViewModelKey(SimulateSummaryViewModel::class)
    abstract fun bindSimulateSummaryViewModel(viewModel: SimulateSummaryViewModel): ViewModel

}