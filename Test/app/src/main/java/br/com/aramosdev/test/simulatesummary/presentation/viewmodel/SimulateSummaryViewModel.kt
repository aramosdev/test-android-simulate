package br.com.aramosdev.test.simulatesummary.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import br.com.aramosdev.test.R
import br.com.aramosdev.test.shared.BaseViewModel
import br.com.aramosdev.test.shared.extention.formatBR
import br.com.aramosdev.test.shared.extention.toMoney
import br.com.aramosdev.test.shared.extention.toPercentage
import br.com.aramosdev.test.simulator.domain.models.Simulate
import javax.inject.Inject

class SimulateSummaryViewModel @Inject constructor(): BaseViewModel() {

    val summaryResult = MutableLiveData<Simulate>()
    val navigationState = MutableLiveData<SimulateSummaryViewModelState>()

    val containerFirst = Transformations.map(summaryResult) {
        mutableListOf(
            R.string.text_invested_amount to it.investmentParameterResponse?.investedAmount.toMoney(),
            R.string.text_gross_amount to it.grossAmount.toMoney(),
            R.string.text_gross_amount_profit to it.grossAmountProfit.toMoney(),
            R.string.text_taxes_amount to it.taxesAmount.toMoney(),
            R.string.text_net_amount to it.netAmount.toMoney(),
        )
    }
    val containerSecond = Transformations.map(summaryResult) {
        mutableListOf(
            R.string.text_maturity_date to it.investmentParameterResponse?.maturityDate?.formatBR(),
            R.string.text_maturity_total_days to it.investmentParameterResponse?.maturityTotalDays.toString(),
            R.string.text_monthly_gross_rate_profit to it.monthlyGrossRateProfit?.toPercentage(),
            R.string.text_rate to it.investmentParameterResponse?.rate?.toPercentage(),
            R.string.text_annual_net_rate_profit to it.annualNetRateProfit?.toPercentage(),
            R.string.text_rate_profit to it.rateProfit?.toPercentage()
        )
    }

    fun onSimulateAgain() {
        navigationState.postValue(SimulateSummaryViewModelState.OpenSimulateAgain)
    }
}