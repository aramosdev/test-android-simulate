package br.com.aramosdev.test.simulator.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Simulate(
    val investmentParameterResponse: InvestmentParameter? = null,
    val grossAmount: Double? = null,
    val taxesAmount: Double? = null,
    val netAmount: Double? = null,
    val grossAmountProfit: Double? = null,
    val netAmountProfit: Double? = null,
    val annualGrossRateProfit: Double? = null,
    val monthlyGrossRateProfit: Double? = null,
    val dailyGrossRateProfit: Double? = null,
    val taxesRate: Double? = null,
    val rateProfit: Double? = null,
    val annualNetRateProfit: Double? = null
): Parcelable
