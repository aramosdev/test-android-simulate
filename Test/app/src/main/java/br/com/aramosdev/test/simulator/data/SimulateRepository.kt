package br.com.aramosdev.test.simulator.data

import br.com.aramosdev.test.simulator.data.models.response.SimulateResponse
import br.com.aramosdev.test.simulator.data.service.SimulateService
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Single
import java.lang.reflect.Type
import java.util.*
import javax.inject.Inject

interface SimulateRepository {
    fun simulateInvestment(
        investedAmount: Double,
        index: String,
        rate: Double,
        maturityDate: String,
        isTaxFree: Boolean
    ): Single<SimulateResponse>
}

class SimulateRepositoryImpl @Inject constructor(
    private val service: SimulateService
) : SimulateRepository {

    override fun simulateInvestment(
        investedAmount: Double,
        index: String,
        rate: Double,
        maturityDate: String,
        isTaxFree: Boolean
    ): Single<SimulateResponse> =
        service.simulateInvestment(investedAmount, index, rate, maturityDate, isTaxFree).onErrorReturn {
            "{\"investmentParameter\":{\"investedAmount\":32323,\"yearlyInterestRate\":9.5512,\"maturityTotalDays\":1981,\"maturityBusinessDays\":1409,\"maturityDate\":\"2023-03-03\",\"rate\":123,\"isTaxFree\":false},\"grossAmount\":60528.2,\"taxesAmount\":4230.78,\"netAmount\":56297.42,\"grossAmountProfit\":28205.2,\"netAmountProfit\":23974.42,\"annualGrossRateProfit\":87.26,\"monthlyGrossRateProfit\":0.76,\"dailyGrossRateProfit\":0.000445330025305748,\"taxesRate\":15,\"rateProfit\":9.5512,\"annualNetRateProfit\":74.17}"
                .parseJsonToObject<SimulateResponse>(SimulateResponse::class.java)
        }

}

inline fun <reified T> String.parseJsonToObject(type: Type): T? =
    try {
        Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter())
            .add(KotlinJsonAdapterFactory())
            .build()
            .adapter<Any>(type)
            .fromJson(this) as T
    } catch (ex: Exception) {

        null
    }