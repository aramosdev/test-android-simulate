package br.com.aramosdev.test.simulator.data.models.response

import com.squareup.moshi.Json

data class SimulateResponse(
    @Json(name = "investmentParameter")
    val investmentParameter: InvestmentParameterResponse? = null,
    @Json(name = "grossAmount")
    val grossAmount: Double? = null,
    @Json(name = "taxesAmount")
    val taxesAmount: Double? = null,
    @Json(name = "netAmount")
    val netAmount: Double? = null,
    @Json(name = "grossAmountProfit")
    val grossAmountProfit: Double? = null,
    @Json(name = "netAmountProfit")
    val netAmountProfit: Double? = null,
    @Json(name = "annualGrossRateProfit")
    val annualGrossRateProfit: Double? = null,
    @Json(name = "monthlyGrossRateProfit")
    val monthlyGrossRateProfit: Double? = null,
    @Json(name = "dailyGrossRateProfit")
    val dailyGrossRateProfit: Double? = null,
    @Json(name = "taxesRate")
    val taxesRate: Double? = null,
    @Json(name = "rateProfit")
    val rateProfit: Double? = null,
    @Json(name = "annualNetRateProfit")
    val annualNetRateProfit: Double? = null
)