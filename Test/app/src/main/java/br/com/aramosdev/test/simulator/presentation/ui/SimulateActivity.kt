package br.com.aramosdev.test.simulator.presentation.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import br.com.aramosdev.test.R
import br.com.aramosdev.test.databinding.ActivitySimulateBinding
import br.com.aramosdev.test.shared.extention.bind
import br.com.aramosdev.test.shared.viewmodel.BaseActivity
import br.com.aramosdev.test.simulatesummary.presentation.ui.SimulateSummaryActivity
import br.com.aramosdev.test.simulator.presentation.viewmodel.SimulateViewModel
import br.com.aramosdev.test.simulator.presentation.viewmodel.SimulateViewModelEvent
import br.com.aramosdev.test.simulator.presentation.viewmodel.SimulateViewModelState
import javax.inject.Inject


class SimulateActivity @Inject constructor() : BaseActivity() {

    private val simulateViewModel by appViewModel<SimulateViewModel>()

    private lateinit var binding: ActivitySimulateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = bind(R.layout.activity_simulate) {
            viewmodel = simulateViewModel
            lifecycleOwner = this@SimulateActivity
        }

        watchState()
        watchEvent()
    }

    private fun watchEvent() {
        simulateViewModel.eventState.observe(this) { event ->
            when (event) {
                is SimulateViewModelEvent.Error.Default -> {
                    showError(getString(R.string.message_error))
                }
            }
        }
    }

    private fun watchState() {
        simulateViewModel.navigationState.observe(this) { state ->
            when (state) {
                is SimulateViewModelState.ShowDetailSimulate -> {
                    SimulateSummaryActivity.startActivity(this, state.simulate, ACTIVITY_RESULT)
                }
            }
        }
    }

    fun showError(message: String) {
        val dialog = AlertDialog.Builder(this)
        dialog.setMessage(message)
        dialog.setPositiveButton(getString(R.string.text_btn_ok), null)
        dialog.create().show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ACTIVITY_RESULT && resultCode == RESULT_OK) {
            binding.inputDate.setValue("")
            binding.inputMoney.setValue("")
            binding.inputRate.setValue("")
        }
    }

    companion object {
        private const val ACTIVITY_RESULT = 9203
    }
}