package br.com.aramosdev.test.components.formatters

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class PercentageFormatter {
    companion object {
        fun mask(fieldEditText: EditText): TextWatcher {
            return object : TextWatcher {
                var isUpdating: Boolean = false

                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    val str = s.toString().replace("%","")

                    if (count == 0) isUpdating = true

                    if (isUpdating) {
                        isUpdating = false
                        return
                    }

                    isUpdating = true

                    val fieldWithMask = "$str%".replace(".", ",")
                    fieldEditText.setText(fieldWithMask)
                    fieldEditText.setSelection(str.length)
                }

                override fun afterTextChanged(editable: Editable) {}
            }
        }
    }
}