package br.com.aramosdev.test.components

import android.content.Context
import android.graphics.Typeface
import android.text.InputType
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import br.com.aramosdev.test.R
import br.com.aramosdev.test.components.formatters.DateFormatter
import br.com.aramosdev.test.components.formatters.DateFormatter.Companion.FORMATTER_DATE
import br.com.aramosdev.test.components.formatters.MoneyFormatter
import br.com.aramosdev.test.components.formatters.PercentageFormatter
import br.com.aramosdev.test.shared.extention.getNumbers
import kotlinx.android.synthetic.main.view_edittext.view.*

class EditTexView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val maskDate by lazy {
        DateFormatter.mask(FORMATTER_DATE, input)
    }

    private val maskPercentage by lazy {
        PercentageFormatter.mask(input)
    }

    private val maskMoney by lazy {
        MoneyFormatter.mask(input)
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.view_edittext, this)
    }

     fun setValue(value: Any?) {
         value?.let {
             input.setText(it.toString())
         }
     }

    fun getValueDouble() = input.text.toString().getNumbers().toDoubleOrNull()

    fun getValue() = input.text.toString()

    fun setType(type: String) {
        when (type) {
            TYPE_TEXT -> {
                input.inputType = InputType.TYPE_CLASS_TEXT
                input.typeface = Typeface.DEFAULT
            }
            TYPE_NUMBER -> {
                input.inputType = InputType.TYPE_CLASS_NUMBER or
                        InputType.TYPE_NUMBER_VARIATION_NORMAL
                input.typeface = Typeface.DEFAULT
            }
            TYPE_MONEY -> {
                input.inputType = InputType.TYPE_CLASS_NUMBER or
                        InputType.TYPE_NUMBER_VARIATION_NORMAL
                input.typeface = Typeface.DEFAULT
                input.addTextChangedListener(maskMoney)
            }
            TYPE_PERCENTAGE -> {
                input.inputType = InputType.TYPE_CLASS_NUMBER or
                        InputType.TYPE_NUMBER_VARIATION_NORMAL
                input.typeface = Typeface.DEFAULT
                input.addTextChangedListener(maskPercentage)
            }
            TYPE_DATE -> {
                input.inputType = InputType.TYPE_CLASS_NUMBER or
                        InputType.TYPE_NUMBER_VARIATION_NORMAL
                input.typeface = Typeface.DEFAULT
                input.addTextChangedListener(maskDate)
            }
        }
    }

    fun getInput(): EditText = input

    interface EditTextViewActionListener {
        fun onTextChanged(isEmpty: Boolean)
    }

    companion object {
        const val TYPE_DATE = "date"
        const val TYPE_TEXT = "text"
        const val TYPE_NUMBER = "number"
        const val TYPE_MONEY = "money"
        const val TYPE_PERCENTAGE = "percentage"
    }
}