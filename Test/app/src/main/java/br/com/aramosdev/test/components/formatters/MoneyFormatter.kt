package br.com.aramosdev.test.components.formatters

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import br.com.aramosdev.test.shared.extention.getNumbers
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class MoneyFormatter {
    companion object {
        const val CURRENCY = "R$ "
        const val NEGATIVE_PREFIX = "- $CURRENCY"

        private val formatter: NumberFormat by lazy {
            NumberFormat.getCurrencyInstance(Locale("pt", "BR")).apply {
                with(this as DecimalFormat) {
                    positivePrefix = CURRENCY
                    negativePrefix = NEGATIVE_PREFIX
                }
            }
        }

        fun mask(fieldEditText: EditText): TextWatcher {
            return object : TextWatcher {
                var isUpdating: Boolean = false

                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    var current = s.toString().getNumbers()

                    if (count == 0 || current.toDoubleOrNull() == null) isUpdating = true

                    if (isUpdating) {
                        isUpdating = false
                        return
                    }

                    if (current.length == 4) {
                        if (current.first() == '0') {
                            current = s.toString().getNumbers().replaceFirst("0", "")
                        }
                    }

                    val aux = when (current.length) {
                        1 -> "00$current"
                        2 -> "0$current"
                        else -> current
                    }

                    val value = aux.reversed().mapIndexed { i, c ->
                        when {
                            i == 2 -> "$c."
                            i < 2 -> c.toString()
                            else -> c.toString()
                        }
                    }.reversed().fold("") { acc, element ->
                        acc.plus(element)
                    }.toDoubleOrNull() ?: current

                    isUpdating = true
                    val format = formatter.format(value)
                    fieldEditText.setText(format)
                    fieldEditText.setSelection(format.length)

                }

                override fun afterTextChanged(editable: Editable) {}
            }
        }
    }
}