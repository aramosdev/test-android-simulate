package br.com.aramosdev.test.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import br.com.aramosdev.test.R
import kotlinx.android.synthetic.main.view_label_value_component.view.*

class LabelValueComponent @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private val adapter by lazy {
        LabelValueAdapter()
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.view_label_value_component, this)

        (listInfo.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        listInfo.isNestedScrollingEnabled = false
        listInfo.layoutManager = LinearLayoutManager(context)
        listInfo.adapter = adapter
        listInfo.visibility = GONE
    }

    fun labelValuesList(labelValues :MutableList<Pair<Int, String?>> ) :LabelValueComponent {
        adapter.addAll(labelValues)
        listInfo.visibility = VISIBLE
        return this
    }
}