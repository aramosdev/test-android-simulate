package br.com.aramosdev.test.components

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class LabelValueAdapter: RecyclerView.Adapter<LabelValueAdapter.LabelValueViewHolder>() {

    private var listLabelValue = mutableListOf<Pair<Int, String?>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LabelValueAdapter.LabelValueViewHolder {
        return LabelValueViewHolder(LabelValueView(parent.context))
    }

    override fun getItemCount() = listLabelValue.size

    fun addAll(labelValue: MutableList<Pair<Int, String?>>) {
        listLabelValue = labelValue
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: LabelValueAdapter.LabelValueViewHolder, position: Int) {
        holder.bind(listLabelValue[position])
    }

    inner class LabelValueViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        lateinit var labelValue: Pair<Int, String?>
        fun bind(item: Pair<Int, String?>) {
            labelValue = item
            val labelValueView = view as LabelValueView

            item.second?.let {
                labelValueView.build(item.first, it)
            }
        }
    }
}