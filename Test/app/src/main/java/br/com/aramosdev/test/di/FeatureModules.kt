package br.com.aramosdev.test.di

import br.com.aramosdev.test.shared.viewmodel.AppViewModelFactoryModule
import br.com.aramosdev.test.simulatesummary.di.SimulateSummaryContributeModule
import br.com.aramosdev.test.simulator.presentation.di.SimulateContributeModule
import dagger.Module


@Module(includes = [
    SimulateContributeModule::class,
    SimulateSummaryContributeModule::class,
    AppViewModelFactoryModule::class
])
class FeatureModules