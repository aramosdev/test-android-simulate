package br.com.aramosdev.test.shared.extention

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun String.validateDate(): Boolean {
    val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    formatter.isLenient = false
    return try {
        formatter.parse(this)
        true
    } catch (_: ParseException) {
        false
    }
}

fun Date.formatBR(fromFormat: String = "dd/MM/yyyy"): String {
    val to = SimpleDateFormat(fromFormat, Locale("pt", "BR"))
    return to.format(this)
}

fun String.formatUS(fromFormat: String = "dd/MM/yyyy", toFormat: String = "yyyy-MM-dd"): String {
    return try {
        val locate = Locale("pt", "BR")
        val from = SimpleDateFormat(fromFormat, locate)
        val to = SimpleDateFormat(toFormat, locate)
        from.parse(this)?.let {
            to.format(it)
        } ?: this
    } catch (e: Exception) {
        this
    }
}