package br.com.aramosdev.test.simulator.data.models.response

import com.squareup.moshi.Json
import java.util.*

data class InvestmentParameterResponse (
    @Json(name = "investedAmount")
    val investedAmount: Double?,
    @Json(name = "yearlyInterestRate")
    val yearlyInterestRate: Double?,
    @Json(name = "maturityTotalDays")
    val maturityTotalDays: Int?,
    @Json(name = "maturityBusinessDays")
    val maturityBusinessDays: Int?,
    @Json(name = "maturityDate")
    val maturityDate: Date?,
    @Json(name = "rate")
    val rate: Double?,
    @Json(name = "isTaxFree")
    val isTaxFree: Boolean?
)