package br.com.aramosdev.test.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import br.com.aramosdev.test.R
import kotlinx.android.synthetic.main.view_label_value.view.*

class LabelValueView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.view_label_value, this)
    }

    fun build(label: Int, value: String) : LabelValueView {
        labelText.apply {
            setText(label)
            visibility = VISIBLE
        }
        valueText.apply {
            text = value
            visibility = VISIBLE
        }
        return this
    }

}