package br.com.aramosdev.test.components.buildingadapter

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import androidx.databinding.BindingAdapter

object TextViewAdapter {
    @BindingAdapter("app:full_text", "app:substring_text", "app:substring_color")
    @JvmStatic
    fun changeColorSubstring(view: TextView, text: String, substring: String, color: Int) {
        val firstMatchingIndex = text.indexOf(substring)
        val lastMatchingIndex = firstMatchingIndex + substring.length
        val spannable = SpannableString(text)
        spannable.setSpan(ForegroundColorSpan(color), firstMatchingIndex, lastMatchingIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        view.text = spannable
    }
}