package br.com.aramosdev.test.simulator.data.models.resquest

data class SimulateParameter(
    var investedAmount: Double? = null,
    var rate: Double? = null,
    var maturityDate: String? = null,
    val index: String = "CDI",
    val isTaxFree: Boolean = false
)
