package br.com.aramosdev.test.components.buildingadapter

import android.view.View
import androidx.databinding.BindingAdapter

object ViewAdapter {
    @BindingAdapter("app:visibleView")
    @JvmStatic
    fun visibleView(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
}