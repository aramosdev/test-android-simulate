package br.com.aramosdev.test.di

import br.com.aramosdev.test.BuildConfig
import br.com.aramosdev.test.shared.network.NetworkModule
import br.com.aramosdev.test.shared.rx.AppSchedulers
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named

@Module(includes = [NetworkModule::class])
class AppModule {

    @Provides
    fun provideAppSchedulers()
            = AppSchedulers(Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    @Named("BaseUrl")
    fun provideBaseUrl(): String =
            BuildConfig.BASE_URL

}