package br.com.aramosdev.test.simulator.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class InvestmentParameter(
    val investedAmount: Double?,
    val yearlyInterestRate: Double?,
    val maturityTotalDays: Int?,
    val maturityBusinessDays: Int?,
    val maturityDate: Date?,
    val rate: Double?,
    val isTaxFree: Boolean?
): Parcelable