package br.com.aramosdev.test.components.buildingadapter

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import br.com.aramosdev.test.components.EditTexView
import kotlinx.android.synthetic.main.view_edittext.view.*


object EditTextAdapter {

    @BindingAdapter("app:label_title")
    @JvmStatic
    fun bindInputTitle(view: EditTexView, title: String) {
        view.label.text = title
    }

    @BindingAdapter("app:input_hint")
    @JvmStatic
    fun bindInputHint(view: EditTexView, hint: String) {
        view.input.hint = hint
    }

    @BindingAdapter("app:input_type")
    @JvmStatic
    fun bindInputType(view: EditTexView, type: String) {
        view.setType(type)
    }

    @JvmStatic
    @BindingAdapter("app:input_value_string")
    fun setInputValueString(view: EditTexView, value: String?) {
        view.setValue(value)
    }

    @InverseBindingAdapter(attribute = "app:input_value_string", event = "editTexViewAttrChanged")
    @JvmStatic
    fun getInputValueString(view: EditTexView): String? {
        return view.getValue()
    }

    @JvmStatic
    @BindingAdapter("app:input_value_double")
    fun setInputValueDouble(view: EditTexView, value: Double?) {
        view.setValue(value)
    }

    @JvmStatic
    @InverseBindingAdapter(attribute = "app:input_value_double", event = "editTexViewAttrChanged")
    fun getInputValueDouble(view: EditTexView): Double? {
        return view.getValueDouble() ?: 0.0
    }

    @SuppressLint("OnEditorActionViewAccessibility")
    @BindingAdapter("app:onEditorAction")
    @JvmStatic
    fun setOnOkInSoftKeyboardListener(
        view: EditTexView,
        onEditTextChanged: EditTexView.EditTextViewActionListener?
    ) {
        if (onEditTextChanged == null) {
            view.getInput().setOnEditorActionListener(null)
        } else {
            view.getInput().addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                override fun afterTextChanged(s: Editable?) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    onEditTextChanged.onTextChanged(s.isNullOrEmpty())
                }
            })

        }
    }

    @BindingAdapter("editTexViewAttrChanged")
    @JvmStatic
    fun setListener(view: EditTexView, listener: InverseBindingListener?) {
        listener?.let {
            view.input.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    listener.onChange()
                }
                override fun afterTextChanged(s: Editable?) {}
            })
        }
    }
}