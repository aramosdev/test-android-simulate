package br.com.aramosdev.test.simulator.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.aramosdev.test.components.EditTexView
import br.com.aramosdev.test.shared.BaseViewModel
import br.com.aramosdev.test.shared.extention.formatUS
import br.com.aramosdev.test.shared.rx.AppSchedulers
import br.com.aramosdev.test.simulator.data.models.resquest.SimulateParameter
import br.com.aramosdev.test.simulator.domain.SimulateInteractor
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SimulateViewModel @Inject constructor(
    private val interactor: SimulateInteractor,
    private val appSchedulers: AppSchedulers
) : BaseViewModel() {

    val loadingVisibility = MutableLiveData(false)
    val request = MutableLiveData(SimulateParameter())
    val isValidateSimulator = MutableLiveData(false)
    val onEditTextChanged = MutableLiveData<EditTexView.EditTextViewActionListener>()
    val eventState = MutableLiveData<SimulateViewModelEvent>()
    val navigationState = MutableLiveData<SimulateViewModelState>()

    override fun onCreate() {
        super.onCreate()
        onEditTextChanged.value = object : EditTexView.EditTextViewActionListener {
            override fun onTextChanged(isEmpty: Boolean) {
                isValidateSimulator.value = false
                if (isEmpty.not()) checkRequestSimulate()
            }
        }
    }

    fun checkRequestSimulate() {
        val value = request.value
        if (value?.maturityDate?.isNotEmpty() == true
            && (value.rate ?: 0.0) > 0.0
            && (value.investedAmount ?: 0.0) > 0.0
        ) {
            isValidateSimulator.value = true
            return
        }
    }

    fun onSimulate() {
        request.value?.let { parameter ->
            val investedAmount = request.value?.investedAmount ?: 0.0
            val rate = request.value?.rate ?: 0.0
            val maturityDate = request.value?.maturityDate?.formatUS()

            if (investedAmount > 0.0 && rate > 0.0 && maturityDate != null) {
                disposable + interactor.simulateInvestment(
                    investedAmount = investedAmount,
                    index = parameter.index,
                    rate = rate,
                    maturityDate = maturityDate,
                    isTaxFree = parameter.isTaxFree
                )
                    .subscribeOn(appSchedulers.ioScheduler)
                    .observeOn(appSchedulers.mainScheduler)
                    .doOnSubscribe {
                        loadingVisibility.postValue(true)
                    }
                    .doFinally {
                        loadingVisibility.postValue(false)
                    }
                    .subscribeBy(
                        onSuccess = {
                            navigationState.postValue(SimulateViewModelState.ShowDetailSimulate(it))
                        },
                        onError = {
                            eventState.postValue(SimulateViewModelEvent.Error.Default)
                        }
                    )
            }
        }
    }
}