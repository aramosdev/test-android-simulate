package br.com.aramosdev.test.shared.extention

fun String.getNumbers() = replace("[^\\d+]".toRegex(), "")