package br.com.aramosdev.test.shared.extention

import android.app.Activity
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

inline fun <reified T: ViewDataBinding> Activity.bind(@LayoutRes layout: Int, noinline block: T.() -> Unit): T {
    return DataBindingUtil.setContentView<T>(this, layout).apply {
        block.invoke(this)
    }
}