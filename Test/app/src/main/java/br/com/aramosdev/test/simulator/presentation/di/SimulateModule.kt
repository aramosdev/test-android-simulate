package br.com.aramosdev.test.simulator.presentation.di

import androidx.lifecycle.ViewModel
import br.com.aramosdev.test.shared.viewmodel.ViewModelKey
import br.com.aramosdev.test.simulator.data.SimulateRepository
import br.com.aramosdev.test.simulator.data.SimulateRepositoryImpl
import br.com.aramosdev.test.simulator.data.service.SimulateService
import br.com.aramosdev.test.simulator.domain.SimulateInteractor
import br.com.aramosdev.test.simulator.domain.SimulateInteractorImpl
import br.com.aramosdev.test.simulator.presentation.ui.SimulateActivity
import br.com.aramosdev.test.simulator.presentation.viewmodel.SimulateViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import retrofit2.Retrofit
import javax.inject.Named

@Module
abstract class SimulateContributeModule {

    @ContributesAndroidInjector(modules = [(SimulateModule::class), (SimulateProvidesModule::class)])
    abstract fun provideSimulateActivity(): SimulateActivity
}


@Module
abstract class SimulateModule {

    @Binds
    @IntoMap
    @ViewModelKey(SimulateViewModel::class)
    abstract fun bindSimulateViewModel(viewModel: SimulateViewModel): ViewModel

}


@Module
class SimulateProvidesModule {
    @Provides
    fun provideSimulateRepository(imp: SimulateRepositoryImpl): SimulateRepository = imp

    @Provides
    fun provideSimulateInteractor(imp: SimulateInteractorImpl): SimulateInteractor = imp

    @Provides
    fun provideService(@Named("RetrofitApi") retrofit: Retrofit): SimulateService
            = retrofit.create(SimulateService::class.java)
}