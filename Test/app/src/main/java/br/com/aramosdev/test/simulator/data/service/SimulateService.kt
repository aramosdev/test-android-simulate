package br.com.aramosdev.test.simulator.data.service

import br.com.aramosdev.test.simulator.data.models.response.SimulateResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SimulateService {
    @GET("v3/ecfaebf5-782b-4b24-ae4f-23b5c3a861da")
    fun simulateInvestment(
        @Query("investedAmount") investedAmount: Double,
        @Query("index") index: String,
        @Query("rate") rate: Double,
        @Query("maturityDate") maturityDate: String,
        @Query("isTaxFree") isTaxFree: Boolean,
    ): Single<SimulateResponse>
}