package br.com.aramosdev.test.simulator.domain.models.mapper

import br.com.aramosdev.test.simulator.data.models.response.InvestmentParameterResponse
import br.com.aramosdev.test.simulator.data.models.response.SimulateResponse
import br.com.aramosdev.test.simulator.domain.models.InvestmentParameter
import br.com.aramosdev.test.simulator.domain.models.Simulate

fun SimulateResponse.toSimulator() = Simulate(
    investmentParameterResponse = investmentParameter?.toInvestmentParameter(),
    grossAmount = grossAmount,
    taxesAmount = taxesAmount,
    netAmount = netAmount,
    grossAmountProfit = grossAmountProfit,
    netAmountProfit = netAmountProfit,
    annualGrossRateProfit = annualGrossRateProfit,
    monthlyGrossRateProfit = monthlyGrossRateProfit,
    dailyGrossRateProfit = dailyGrossRateProfit,
    taxesRate = taxesRate,
    rateProfit = rateProfit,
    annualNetRateProfit = annualNetRateProfit
)

fun InvestmentParameterResponse.toInvestmentParameter() = InvestmentParameter(
    investedAmount = investedAmount,
    yearlyInterestRate = yearlyInterestRate,
    maturityTotalDays = maturityTotalDays,
    maturityBusinessDays = maturityBusinessDays,
    maturityDate = maturityDate,
    rate = rate,
    isTaxFree = isTaxFree
)