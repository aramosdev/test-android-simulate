package br.com.aramosdev.test.shared.network

import android.app.Application
import br.com.aramosdev.test.shared.rx.AppSchedulers
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    @Named("ChuckInterceptor")
    fun provideChuckInterceptor(context: Application): Interceptor = ChuckInterceptor(context)

    @Provides
    @Named("OkHttpClientBuilder")
    fun providesDefaultOkHttpBuilder(
        @Named("ChuckInterceptor") chuckInterceptor: Interceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(chuckInterceptor)
            .build()
    }

    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter())
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    @Singleton
    @Provides
    @Named("RetrofitApi")
    fun provideRetrofitOverturned(
        @Named("OkHttpClientBuilder") oktHttpClient: OkHttpClient,
        @Named("BaseUrl") baseAuthUrl: String,
        moshi: Moshi,
        appSchedulers: AppSchedulers
    ): Retrofit =
        Retrofit.Builder()
            .client(oktHttpClient)
            .baseUrl(baseAuthUrl)
            .addConverterFactory(NullOnEmptyConverterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(appSchedulers.ioScheduler))
            .build()
}