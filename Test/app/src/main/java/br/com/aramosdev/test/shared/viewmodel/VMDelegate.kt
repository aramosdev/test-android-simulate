package br.com.aramosdev.test.shared.viewmodel

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import br.com.aramosdev.test.shared.BaseViewModel
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty


class VMDelegate<T: BaseViewModel>(
    private val clazz: KClass<T>,
    private val activity: FragmentActivity,
    private val vmFactory: () -> ViewModelProvider.Factory
) : ReadOnlyProperty<FragmentActivity, T> {

    var cache: T? = null

    override fun getValue(thisRef: FragmentActivity, property: KProperty<*>): T {
        return cache?.let {
            it
        } ?: run {
            ViewModelProvider(activity, vmFactory.invoke()).get(clazz.java).apply {
                thisRef.lifecycle.addObserver(this)
            }.also {
                cache = it
            }
        }
    }

}