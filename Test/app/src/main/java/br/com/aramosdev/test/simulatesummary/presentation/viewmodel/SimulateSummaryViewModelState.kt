package br.com.aramosdev.test.simulatesummary.presentation.viewmodel

sealed class SimulateSummaryViewModelState {
    object OpenSimulateAgain: SimulateSummaryViewModelState()
}