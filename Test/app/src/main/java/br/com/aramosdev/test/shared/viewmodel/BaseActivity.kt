package br.com.aramosdev.test.shared.viewmodel

import br.com.aramosdev.test.shared.BaseViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

open class BaseActivity: DaggerAppCompatActivity() {

    @Inject
    lateinit var vmFactory: AppViewModelFactory

    inline fun <reified VM : BaseViewModel> appViewModel(): VMDelegate<VM> {
        return VMDelegate(VM::class, this) { this.vmFactory }
    }

}