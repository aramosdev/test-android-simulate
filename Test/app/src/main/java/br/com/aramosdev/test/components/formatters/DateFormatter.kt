package br.com.aramosdev.test.components.formatters

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class DateFormatter {
    companion object {

        const val FORMATTER_DATE = "##/##/####"

        fun replaceChars(fieldFull: String): String {
            return fieldFull
                .replace("/", "")
        }

        fun mask(mask: String, fieldEditText: EditText): TextWatcher {
            return object : TextWatcher {
                var isUpdating: Boolean = false
                var oldString: String = ""

                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    val str = replaceChars(s.toString())
                    var fieldWithMask = ""

                    if (count == 0) isUpdating = true

                    if (isUpdating) {
                        oldString = str
                        isUpdating = false
                        return
                    }

                    var i = 0
                    for (m: Char in mask.toCharArray()) {
                        if (m != '#' && str.length > oldString.length) {
                            fieldWithMask += m
                            continue
                        }
                        try {
                            fieldWithMask += str.get(i)
                        } catch (e: Exception) {
                            break
                        }
                        i++
                    }

                    isUpdating = true
                    fieldEditText.setText(fieldWithMask)
                    fieldEditText.setSelection(fieldWithMask.length)

                }

                override fun afterTextChanged(editable: Editable) {}
            }
        }
    }
}