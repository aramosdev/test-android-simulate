package br.com.aramosdev.test.components.buildingadapter

import androidx.databinding.BindingAdapter
import br.com.aramosdev.test.components.LabelValueComponent

object LabelValueComponent {
    @BindingAdapter("app:bindingItems")
    @JvmStatic
    fun <T> bindingItems(view: LabelValueComponent, items: MutableList<Pair<Int, String?>>?) {
        items?.let {
            view.labelValuesList(it)
        }
    }
}