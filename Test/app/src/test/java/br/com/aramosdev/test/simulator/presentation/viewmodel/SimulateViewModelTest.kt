package br.com.aramosdev.test.simulator.presentation.viewmodel

import androidx.lifecycle.Observer
import br.com.aramosdev.test.BaseViewModelTest
import br.com.aramosdev.test.components.EditTexView
import br.com.aramosdev.test.emitted
import br.com.aramosdev.test.notEmitted
import br.com.aramosdev.test.simulator.data.models.resquest.SimulateParameter
import br.com.aramosdev.test.simulator.domain.SimulateInteractor
import br.com.aramosdev.test.simulator.domain.models.Simulate
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.spyk
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class SimulateViewModelTest : BaseViewModelTest() {
    @MockK
    private lateinit var interactor: SimulateInteractor

    @RelaxedMockK
    private lateinit var loadingVisibility: Observer<Boolean>

    @RelaxedMockK
    private lateinit var request: Observer<SimulateParameter>

    @RelaxedMockK
    private lateinit var isValidateSimulator: Observer<Boolean>

    @RelaxedMockK
    private lateinit var onEditTextChanged: Observer<EditTexView.EditTextViewActionListener>

    @RelaxedMockK
    private lateinit var eventState: Observer<SimulateViewModelEvent>

    @RelaxedMockK
    private lateinit var navigationState: Observer<SimulateViewModelState>


    private lateinit var viewModel: SimulateViewModel

    @Before
    fun setup() {
        MockKAnnotations.init(this)

        viewModel = spyk(SimulateViewModel(interactor, appSchedulers))
        setupObserver()
    }

    private fun setupObserver() {
        viewModel.navigationState.observeForever(navigationState)
        viewModel.eventState.observeForever(eventState)
        viewModel.loadingVisibility.observeForever(loadingVisibility)
        viewModel.request.observeForever(request)
        viewModel.isValidateSimulator.observeForever(isValidateSimulator)
        viewModel.onEditTextChanged.observeForever(onEditTextChanged)
    }

    @Test
    fun `should enable button when parameter complete`() {
        val date = "12/03/2024"
        val rateResponse = 10.0
        val investedAmountResponse = 100.0
        val requestResponse: SimulateParameter = mockk {
            every { maturityDate } returns date
            every { rate } returns rateResponse
            every { investedAmount } returns investedAmountResponse
        }

        viewModel.request.value = requestResponse

        viewModel.checkRequestSimulate()

        isValidateSimulator emitted true
    }

    @Test
    fun `should disable button when parameter maturityDate is null`() {
        val rateResponse = 10.0
        val investedAmountResponse = 100.0
        val requestResponse: SimulateParameter = mockk {
            every { maturityDate } returns null
            every { rate } returns rateResponse
            every { investedAmount } returns investedAmountResponse
        }

        viewModel.request.value = requestResponse

        viewModel.checkRequestSimulate()

        isValidateSimulator emitted false
    }

    @Test
    fun `should disable button when parameter rate is null`() {
        val date = "12/03/2024"
        val investedAmountResponse = 100.0
        val requestResponse: SimulateParameter = mockk {
            every { maturityDate } returns date
            every { rate } returns null
            every { investedAmount } returns investedAmountResponse
        }

        viewModel.request.value = requestResponse

        viewModel.checkRequestSimulate()

        isValidateSimulator emitted false
    }

    @Test
    fun `should disable button when parameter investedAmount is null`() {
        val date = "12/03/2024"
        val rateResponse = 10.0
        val requestResponse: SimulateParameter = mockk {
            every { maturityDate } returns date
            every { rate } returns rateResponse
            every { investedAmount } returns null
        }

        viewModel.request.value = requestResponse

        viewModel.checkRequestSimulate()

        isValidateSimulator emitted false
    }

    @Test
    fun `should simulate when response simulate success`() {
        val date = "12/03/2024"
        val rateResponse = 10.0
        val indexResponse = "CDI"
        val isTaxFreeResponse = false
        val investedAmountResponse = 100.0
        val requestResponse: SimulateParameter = mockk {
            every { maturityDate } returns date
            every { rate } returns rateResponse
            every { index } returns indexResponse
            every { isTaxFree } returns isTaxFreeResponse
            every { investedAmount } returns investedAmountResponse
        }
        viewModel.request.value = requestResponse
        val response: Simulate = mockk()
        every {
            interactor.simulateInvestment(
                investedAmount = investedAmountResponse,
                index = indexResponse,
                rate = rateResponse,
                maturityDate = "2024-03-12",
                isTaxFree = isTaxFreeResponse
            )
        } returns Single.just(response)

        viewModel.onSimulate()

        loadingVisibility emitted true
        loadingVisibility emitted false

        navigationState emitted SimulateViewModelState.ShowDetailSimulate(response)
    }

    @Test
    fun `should simulate when response simulate error`() {
        val date = "12/03/2024"
        val rateResponse = 10.0
        val indexResponse = "CDI"
        val isTaxFreeResponse = false
        val investedAmountResponse = 100.0
        val requestResponse: SimulateParameter = mockk {
            every { maturityDate } returns date
            every { rate } returns rateResponse
            every { index } returns indexResponse
            every { isTaxFree } returns isTaxFreeResponse
            every { investedAmount } returns investedAmountResponse
        }
        viewModel.request.value = requestResponse
        val response: Simulate = mockk()
        every {
            interactor.simulateInvestment(
                investedAmount = investedAmountResponse,
                index = indexResponse,
                rate = rateResponse,
                maturityDate = "2024-03-12",
                isTaxFree = isTaxFreeResponse
            )
        } returns Single.error(NullPointerException())

        viewModel.onSimulate()

        loadingVisibility emitted true
        loadingVisibility emitted false

        eventState emitted SimulateViewModelEvent.Error.Default

        navigationState notEmitted  SimulateViewModelState.ShowDetailSimulate(response)
    }
}