package br.com.aramosdev.test.shared.extention

import com.google.common.truth.Truth
import org.junit.Test

class DoubleExtensionKtTest {

    @Test
    fun `should convert value to money BR`() {
        val value = 3000.0

        Truth.assertThat(value.toMoney()).contains("R$ 3.000,00")
    }

    @Test
    fun `should convert value to negative money BR`() {
        val value = - 3000.0

        Truth.assertThat(value.toMoney()).contains("- R$ 3.000,00")
    }

    @Test
    fun `should convert value when value is null`() {
        val value: Double? = null
        Truth.assertThat(value.toMoney()).contains("R$ 0,00")
    }

}