package br.com.aramosdev.test.shared.extention

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class DateExtensionKtTest {

    @Test
    fun `should valid if date is true`() {
        val dateString = "26/12/2021"

        assertThat(dateString.validateDate()).isTrue()
    }

    @Test
    fun `should valid if date is false`() {
        val dateString = "23/23/2323"

        assertThat(dateString.validateDate()).isFalse()
    }

    @Test
    internal fun `should format date BR to US `() {
        val dateString = "26/12/2021"

        assertThat(dateString.formatUS()).contains("2021-12-26")
    }

    @Test
    internal fun `should return original date when format date BR with invalid`() {
        val dateString = "23/12-2021"

        assertThat(dateString.formatUS()).contains("23/12-2021")
    }


}