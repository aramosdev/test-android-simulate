package br.com.aramosdev.test

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import br.com.aramosdev.test.shared.rx.AppSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
open class BaseViewModelTest {
    @Rule
    @JvmField
    var instantExecutorRule = InstantTaskExecutorRule()
    protected val appSchedulers = AppSchedulers(ioScheduler = Schedulers.trampoline(), mainScheduler = Schedulers.trampoline())

    protected val scheduler = TestScheduler()

}

infix fun <T> Observer<T>.emitted(value: T) {
    io.mockk.verify { onChanged(value) }
}
infix fun <T> Observer<T>.notEmitted(value: T) {
    io.mockk.verify(exactly = 0) { onChanged(value) }
}