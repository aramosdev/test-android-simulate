package br.com.aramosdev.test.simulatesummary.presentation

import androidx.lifecycle.Observer
import br.com.aramosdev.test.BaseViewModelTest
import br.com.aramosdev.test.emitted
import br.com.aramosdev.test.simulatesummary.presentation.viewmodel.SimulateSummaryViewModel
import br.com.aramosdev.test.simulatesummary.presentation.viewmodel.SimulateSummaryViewModelState
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.spyk
import org.junit.Before
import org.junit.Test

class SimulateSummaryViewModelTest: BaseViewModelTest() {
    private lateinit var viewModel: SimulateSummaryViewModel
    @RelaxedMockK
    private lateinit var navigationState: Observer<SimulateSummaryViewModelState>

    @Before
    fun setup() {
        MockKAnnotations.init(this)

        viewModel = spyk(SimulateSummaryViewModel())
        setupObserver()
    }

    private fun setupObserver() {
        viewModel.navigationState.observeForever(navigationState)
    }

    @Test
    fun `should on simulate`() {
        viewModel.onSimulateAgain()

        navigationState emitted SimulateSummaryViewModelState.OpenSimulateAgain
    }
}