package br.com.aramosdev.test.shared.extention

import com.google.common.truth.Truth
import org.junit.Test


class StringExtensionsKtTest {

    @Test
    fun `should get numbers`() {
        val value = "apa30s00/.0"

        Truth.assertThat(value.getNumbers()).contains("3000")
    }

    @Test
    fun `should get numbers when not number`() {
        val value = "apas/."

        Truth.assertThat(value.getNumbers()).contains("")
    }
}